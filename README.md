This started as figuring out why deserializing a Date in [temporenc](https://github.com/temporenc/temporenc-rust) was slower than other structs, even structs that had more data. 

The workload performed in these benchmarks is a cut-down version of the deserializatin logic: read 3 bytes with `Read.read_exact` from a `Vec<u8>` into a local array, then return a struct whose fields (all `Options`) are all set to `None`. That's done 100 times each benchmark iteration to make it long enough that it's not in single-digit nanoseconds.

Things that vary between benchmarks:

- How many fields are in the struct (all of which are wrapped in Option): 1x `u16` and 2x `u8`, 3x `u8`, or 1x `u16` and 5x `u8`. (These represent Date, Time, and DateTime.)
- Mapping I/O errors into two different enum types. `FancyError` has two variant, one that holds `ErrorKind` data and another variant that does not. `PlainError` has two variant, neither of which has any extra data.
- If errors are mapped into `FancyError`, whether or not they're mapped into the enum variant that has an `ErrorKind` or the one that does not.
- Returning the struct inside a `Result::Ok`, or just as a bare struct. In the former case, `unwrap()` is called in the benchmark on the result of the benchmarked function. In the latter case, `unwrap()` is called inside the function on the result of the `read_exact`.

I get results like this on an i7-6850K:

```
running 15 tests
test read_1xu16_2xu8_fancy_error_bare_error_kind      ... bench:         375 ns/iter (+/- 0)
test read_1xu16_2xu8_fancy_error_bare_no_error_kind   ... bench:         406 ns/iter (+/- 9)
test read_1xu16_2xu8_fancy_error_result_error_kind    ... bench:         817 ns/iter (+/- 1)
test read_1xu16_2xu8_fancy_error_result_no_error_kind ... bench:         738 ns/iter (+/- 0)
test read_1xu16_2xu8_plain_error_result               ... bench:         375 ns/iter (+/- 0)
test read_1xu16_5xu8_fancy_error_bare_error_kind      ... bench:         406 ns/iter (+/- 9)
test read_1xu16_5xu8_fancy_error_bare_no_error_kind   ... bench:         404 ns/iter (+/- 9)
test read_1xu16_5xu8_fancy_error_result_error_kind    ... bench:         408 ns/iter (+/- 12)
test read_1xu16_5xu8_fancy_error_result_no_error_kind ... bench:         454 ns/iter (+/- 0)
test read_1xu16_5xu8_plain_error_result               ... bench:         375 ns/iter (+/- 0)
test read_3xu8_fancy_error_bare_error_kind            ... bench:         375 ns/iter (+/- 0)
test read_3xu8_fancy_error_bare_no_error_kind         ... bench:         405 ns/iter (+/- 10)
test read_3xu8_fancy_error_result_error_kind          ... bench:         405 ns/iter (+/- 8)
test read_3xu8_fancy_error_result_no_error_kind       ... bench:         375 ns/iter (+/- 0)
test read_3xu8_plain_error_result                     ... bench:         408 ns/iter (+/- 13)

test result: ok. 0 passed; 0 failed; 0 ignored; 15 measured
```

- Two of those benchmarks are about 2x slower than the rest
- All the other benchmarks are about the same (within 5-10% of each other)
- No consistent effect is seen when varying any one factor: struct size, `Result` or not, which `enum` type is used, which variant of the enum is used.
- However, performance is 2x worse when the following are true:
    - The struct is "1x `u16`, 2x `u8`"
    - The function returns a `Result` to expose I/O errors
    - The error type in the `Result` is `FancyError`, regardless of whether or not the enum variant used is the one that has `ErrorKind` data

### Addendum: structs without `Option` fields

Though in practice I need the struct's fields to be `Option`s, on the branch `struct-bare-int-fields` the structs have been changed to just have `u8`, etc, instead of `Option<u8>` and they are all populated with `0` instead of `None`. This changes the performance:

```
test read_1xu16_2xu8_fancy_error_bare_error_kind      ... bench:         375 ns/iter (+/- 1)
test read_1xu16_2xu8_fancy_error_bare_no_error_kind   ... bench:         402 ns/iter (+/- 13)
test read_1xu16_2xu8_fancy_error_result_error_kind    ... bench:         375 ns/iter (+/- 2)
test read_1xu16_2xu8_fancy_error_result_no_error_kind ... bench:         375 ns/iter (+/- 2)
test read_1xu16_2xu8_plain_error_result               ... bench:         375 ns/iter (+/- 2)
test read_1xu16_5xu8_fancy_error_bare_error_kind      ... bench:         408 ns/iter (+/- 20)
test read_1xu16_5xu8_fancy_error_bare_no_error_kind   ... bench:         404 ns/iter (+/- 16)
test read_1xu16_5xu8_fancy_error_result_error_kind    ... bench:         817 ns/iter (+/- 1)
test read_1xu16_5xu8_fancy_error_result_no_error_kind ... bench:         870 ns/iter (+/- 0)
test read_1xu16_5xu8_plain_error_result               ... bench:         375 ns/iter (+/- 5)
test read_3xu8_fancy_error_bare_error_kind            ... bench:         375 ns/iter (+/- 3)
test read_3xu8_fancy_error_bare_no_error_kind         ... bench:         403 ns/iter (+/- 9)
test read_3xu8_fancy_error_result_error_kind          ... bench:         405 ns/iter (+/- 5)
test read_3xu8_fancy_error_result_no_error_kind       ... bench:         375 ns/iter (+/- 0)
test read_3xu8_plain_error_result                     ... bench:         404 ns/iter (+/- 13)
```

Now, instead of the "1xu16, 2xu8" struct being the slow one when combined with `FancyError`, etc, it's the "1xu16, 5xu8".
