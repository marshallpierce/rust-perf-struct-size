use std::io::Read;
use std::io::ErrorKind;

#[allow(dead_code)]
pub struct OneU16TwoU8 {
    f0: Option<u16>,
    f1: Option<u8>,
    f2: Option<u8>
}

#[allow(dead_code)]
pub struct ThreeU8 {
    f0: Option<u8>,
    f1: Option<u8>,
    f2: Option<u8>
}

#[allow(dead_code)]
pub struct OneU16FiveU8 {
    f0: Option<u16>,
    f1: Option<u8>,
    f2: Option<u8>,
    f3: Option<u8>,
    f4: Option<u8>,
    f5: Option<u8>
}

#[derive(Debug)]
pub enum FancyError {
    WithErrorKind(ErrorKind),
    NoErrorKind
}

#[derive(Debug)]
pub enum PlainError {
    ThisError,
    ThatError
}

pub const BUF_SIZE: usize = 3;

// return the struct inside a Result, and include ErrorKind in errors

pub fn read_struct_u16_2xu8_result_error_kind<R: Read>(reader: &mut R) -> Result<OneU16TwoU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind()))?;

    Ok(OneU16TwoU8 {
        f0: None,
        f1: None,
        f2: None
    })
}

pub fn read_struct_3xu8_result_error_kind<R: Read>(reader: &mut R) -> Result<ThreeU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind()))?;

    Ok(ThreeU8 {
        f0: None,
        f1: None,
        f2: None
    })
}

pub fn read_struct_u16_5xu8_result_error_kind<R: Read>(reader: &mut R) -> Result<OneU16FiveU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind()))?;

    Ok(OneU16FiveU8 {
        f0: None,
        f1: None,
        f2: None,
        f3: None,
        f4: None,
        f5: None
    })
}

// return the struct inside a Result, without getting ErrorKind

pub fn read_struct_u16_2xu8_result_no_error_kind<R: Read>(reader: &mut R) -> Result<OneU16TwoU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind)?;

    Ok(OneU16TwoU8 {
        f0: None,
        f1: None,
        f2: None
    })
}

pub fn read_struct_3xu8_result_no_error_kind<R: Read>(reader: &mut R) -> Result<ThreeU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind)?;

    Ok(ThreeU8 {
        f0: None,
        f1: None,
        f2: None
    })
}


pub fn read_struct_u16_5xu8_result_no_error_kind<R: Read>(reader: &mut R) -> Result<OneU16FiveU8, FancyError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind)?;

    Ok(OneU16FiveU8 {
        f0: None,
        f1: None,
        f2: None,
        f3: None,
        f4: None,
        f5: None
    })
}

// return the struct without a Result, without getting ErrorKind

pub fn read_struct_u16_2xu8_bare_no_error_kind<R: Read>(reader: &mut R) -> OneU16TwoU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind).unwrap();

    OneU16TwoU8 {
        f0: None,
        f1: None,
        f2: None
    }
}

pub fn read_struct_3xu8_bare_no_error_kind<R: Read>(reader: &mut R) -> ThreeU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind).unwrap();

    ThreeU8 {
        f0: None,
        f1: None,
        f2: None
    }
}


pub fn read_struct_u16_5xu8_bare_no_error_kind<R: Read>(reader: &mut R) -> OneU16FiveU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| FancyError::NoErrorKind).unwrap();

    OneU16FiveU8 {
        f0: None,
        f1: None,
        f2: None,
        f3: None,
        f4: None,
        f5: None
    }
}

// return the struct without a result, with an error enum with ErrorKind

pub fn read_struct_u16_2xu8_bare_error_kind<R: Read>(reader: &mut R) -> OneU16TwoU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind())).unwrap();

    OneU16TwoU8 {
        f0: None,
        f1: None,
        f2: None
    }
}

pub fn read_struct_3xu8_bare_error_kind<R: Read>(reader: &mut R) -> ThreeU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind())).unwrap();

    ThreeU8 {
        f0: None,
        f1: None,
        f2: None
    }
}


pub fn read_struct_u16_5xu8_bare_error_kind<R: Read>(reader: &mut R) -> OneU16FiveU8 {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|e| FancyError::WithErrorKind(e.kind())).unwrap();

    OneU16FiveU8 {
        f0: None,
        f1: None,
        f2: None,
        f3: None,
        f4: None,
        f5: None
    }
}

// return the struct inside a Result, but with an error type where no options have ErrorKind

pub fn read_struct_u16_2xu8_result_plain_error<R: Read>(reader: &mut R) -> Result<OneU16TwoU8, PlainError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| PlainError::ThisError)?;

    Ok(OneU16TwoU8 {
        f0: None,
        f1: None,
        f2: None
    })
}

pub fn read_struct_3xu8_result_plain_error<R: Read>(reader: &mut R) -> Result<ThreeU8, PlainError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| PlainError::ThisError)?;

    Ok(ThreeU8 {
        f0: None,
        f1: None,
        f2: None
    })
}

pub fn read_struct_u16_5xu8_result_plain_error<R: Read>(reader: &mut R) -> Result<OneU16FiveU8, PlainError> {
    let mut buf = [0; BUF_SIZE];
    reader.read_exact(&mut buf).map_err(|_| PlainError::ThisError)?;

    Ok(OneU16FiveU8 {
        f0: None,
        f1: None,
        f2: None,
        f3: None,
        f4: None,
        f5: None
    })
}
