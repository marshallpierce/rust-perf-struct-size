#![feature(test)]

extern crate perf_struct_size;
extern crate test;

use std::io::Cursor;
use perf_struct_size::*;

const NUM_ROUNDS: usize = 100;

use test::Bencher;

#[bench]
fn read_1xu16_2xu8_fancy_error_result_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_2xu8_result_error_kind(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_3xu8_fancy_error_result_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_3xu8_result_error_kind(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_1xu16_5xu8_fancy_error_result_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_5xu8_result_error_kind(&mut cursor).unwrap();
        }
    })
}


#[bench]
fn read_1xu16_2xu8_fancy_error_result_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_2xu8_result_no_error_kind(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_3xu8_fancy_error_result_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_3xu8_result_no_error_kind(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_1xu16_5xu8_fancy_error_result_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_5xu8_result_no_error_kind(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_1xu16_2xu8_fancy_error_bare_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_2xu8_bare_no_error_kind(&mut cursor);
        }
    })
}

#[bench]
fn read_3xu8_fancy_error_bare_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_3xu8_bare_no_error_kind(&mut cursor);
        }
    })
}

#[bench]
fn read_1xu16_5xu8_fancy_error_bare_no_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_5xu8_bare_no_error_kind(&mut cursor);
        }
    })
}


#[bench]
fn read_1xu16_2xu8_fancy_error_bare_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_2xu8_bare_error_kind(&mut cursor);
        }
    })
}

#[bench]
fn read_3xu8_fancy_error_bare_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_3xu8_bare_error_kind(&mut cursor);
        }
    })
}

#[bench]
fn read_1xu16_5xu8_fancy_error_bare_error_kind(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_5xu8_bare_error_kind(&mut cursor);
        }
    })
}

#[bench]
fn read_1xu16_2xu8_plain_error_result(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_2xu8_result_plain_error(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_3xu8_plain_error_result(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_3xu8_result_plain_error(&mut cursor).unwrap();
        }
    })
}

#[bench]
fn read_1xu16_5xu8_plain_error_result(b: &mut Bencher) {
    let v = fill_vec();
    b.iter(|| {
        let mut cursor = Cursor::new(v.as_slice());
        for _ in 0..NUM_ROUNDS {
            read_struct_u16_5xu8_result_plain_error(&mut cursor).unwrap();
        }
    })
}

fn fill_vec() -> Vec<u8> {
    let mut v: Vec<u8> = Vec::with_capacity(NUM_ROUNDS * BUF_SIZE);

    for _ in 0..(NUM_ROUNDS * BUF_SIZE) {
        v.push(0);
    };

    v
}
